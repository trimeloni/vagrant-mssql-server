# Vagrant - SQL Server on Ubuntu Linux

The following document was used to create the bootstrap script: https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-ubuntu

## Features

* Ubuntu 16.04
* [SQL Server 2017 on Linux](https://docs.microsoft.com/en-us/sql/linux/) (official packages by Microsoft)
* [SQL Server command-line tools on Linux](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools)
* Pre-configured with
  * IP of Vagrant Box: 33.33.33.110
    * All Ports Accessible
  * Vagrant default user: `vagrant` with password `vagrant`
  * Port forwarding from host `2433` to guest `1433` (default).  (still there, no longer needed)
  * Database user `sa` with password `Password123`.
  * Database `master`.
  * Set SQL Server edition to `Developer` with `MSSQL_PID="Developer`.
  * Other of configuration [Environment variable](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-configure-environment-variables) set to default.

## Requirements

* [VirtualBox](https://www.virtualbox.org/) installed.
* [Vagrant](https://www.vagrantup.com/downloads.html) installed.
* If running Win 7, may need to update Powershell

## Installation

* `git clone` this repository
* Follow the [Usage](#usage) section.

## Usage

### Vagrant VM

* `vagrant up` to create and boot the guest virtual machine.
First time run, this may take quite a while as the base box image is downloaded
and provisioned, packages installed.

* `vagrant ssh` to get direct access to the guest shell via SSH.
You'll be connected as the vagrant user.
You can get root access with `sudo` command.

* `vagrant halt` to shutdown the guest machine.

* `vagrant destroy` to wipe out the guest machine completely.
You can re-create it and start over with `vagrant up`.

### SQL Server

Using [sqlcmd](https://docs.microsoft.com/en-us/sql/tools/sqlcmd-utility):

* Connect to SQL Server instance from inside the guest VM

```
vagrant ssh
sqlcmd -S localhost -U SA -P 'Password123' -Q "SELECT @@version;"
sqlcmd -S localhost -U SA -P 'Password123' -Q "SELECT name FROM sys.databases;"
```

* Connect to SQL Server instance from host

```
sqlcmd -S localhost,2433 -U SA -P 'Password123' -Q "SELECT name FROM sys.databases;"
```

## Note
Originally started this project based off of the following:  https://github.com/mloskot/vagrant-sqlserver
Upon using it, nearly the entire vagrant file and configuration scripts were re-written due to the following:
* I tend to follow the same style with my vagrant boxes (i.e. IP Address, Network Attributes, Memory, CPU, etc)
* The original did not work due to SQL Server the original uses was out of date
I HAVE kept nearly this entire Readme.md with some minor changes to reflect my script changes.


#!/usr/bin/env bash
# Part of Vagrant virtual environments for SQL Server 2017 on Ubuntu Linux
#
# Setup environment configuration
export ACCEPT_EULA="Y"
export MSSQL_PID="Developer"
export MSSQL_SA_PASSWORD="Password123"
export DEBIAN_FRONTEND="noninteractive"

# Ensure hostname is recognised
sed -i "s/^127\.0\.0\.1.*/127.0.0.1 localhost $HOSTNAME/g" /etc/hosts

# Install pre-requisites
apt-get -y -q update
apt-get -y -q install curl

## Install SQL Server ##
# Import the GPG Key
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

#Register the SQL Server Repository
add-apt-repository "$(wget -qO- https://packages.microsoft.com/config/ubuntu/16.04/mssql-server-2017.list)"

#Run the following commands to install SQL Server:
apt-get -y -q update
apt-get -y -q install mssql-server

# Post-installation
echo "SQLServer: running /opt/mssql/bin/mssql-conf -n setup"
echo "SQLServer: MSSQL_PID=$MSSQL_PID"
echo "SQLServer: MSSQL_SA_PASSWORD=$MSSQL_SA_PASSWORD"
/opt/mssql/bin/mssql-conf -n setup
/opt/mssql/bin/mssql-conf set telemetry.customerfeedback false

# Show SQL Server is Running
systemctl status mssql-server


## Install SQL Server command-line tools##
# Already added above: wget -qO- https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
add-apt-repository "$(wget -qO- https://packages.microsoft.com/config/ubuntu/16.04/prod.list)"
apt-get -y -q update
apt-get -y -q install mssql-tools unixodbc-dev

# Clean up
apt-get -y -q autoremove
apt-get -y -q clean

echo "Bootstrap: Done installing SQL Server and Tools"
